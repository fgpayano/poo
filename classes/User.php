<?php

class User 
{
	private $arr;
	private $serie;

	public function __construct(array $arr = array(), Pelicula $serie = null)
	{
		$this->serie = $serie;
		$this->arr = $arr;
	}

	public function getArr()
	{
		return $this->arr;
	}

	public function getName ()
	{
		return $this->arr["name"];
	}

	public function drink($juice)
	{
		return $this->getName()." want to drinks ".$juice. " juice </br> and want to see a ".$this->serie->showType();
	}
}





