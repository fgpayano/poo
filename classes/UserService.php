<?php

class UserService 
{
	private $atts;	
	private $serie;

	public function __construct ($atts, Pelicula $serie)
	{
		$this->atts = $atts;
		$this->serie = $serie;
	}

	public function user ()
	{
		return new User($this->atts, $this->serie);
	}
}