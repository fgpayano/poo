<?php

require "User.php";
require "UserService.php";
require "Anime.php";
require "Pelicula.php";
require "Hero.php";

interface SeriesInterface
{
	public function showType();
}

class SeriesController 
{
	private $auth;

	public function __construct(UserService $user = null)
	{
		$this->auth = $user;
	}	

	public function action()
	{
		$user = $this->auth->user();

		if (is_array($user->getArr()))
		{
			echo $user->drink("Pear");
			echo "</br>";
		}
	}
}

$controller = new SeriesController(new UserService(["name" =>"Francis"], new Pelicula()));

$controller->action();
// Testing: Added git tag to test this feature...